/**
 * @fileoverview Prefer e0(&#39;trim&#39;)(x) to x.trim()
 * @author brekk
 */
"use strict"

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var rule = require("../../../lib/rules/method-invocation"),
  RuleTester = require("eslint").RuleTester

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

var ruleTester = new RuleTester()
ruleTester.run("method-invocation", rule, {
  valid: [
    // give me some code that won't trigger a warning
    { code: `e0('trim')` },
    { code: `e1('split', '!')` }
  ],

  invalid: [
    {
      code: "x.trim()",
      errors: [
        {
          message: "Unexpected nullary method. Consider using e0 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.split('.')",
      errors: [
        {
          message: "Unexpected unary method. Consider using e1 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.reduce(function(a) { return [a, b] }, [])",
      errors: [
        {
          message: "Unexpected binary method. Consider using e2 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3)",
      errors: [
        {
          message: "Unexpected ternary method. Consider using e3 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4)",
      errors: [
        {
          message: "Unexpected quaternary method. Consider using e4 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4,5)",
      errors: [
        {
          message: "Unexpected quinary method. Consider using e5 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4,5,6)",
      errors: [
        {
          message: "Unexpected senary method. Consider using e6 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4,5,6,7)",
      errors: [
        {
          message: "Unexpected septenary method. Consider using e7 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4,5,6,7,8)",
      errors: [
        {
          message: "Unexpected octonary method. Consider using e8 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke(1,2,3,4,5,6,7,8,9)",
      errors: [
        {
          message: "Unexpected novenary method. Consider using e9 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.invoke('1','2','3','4','5','6','7','8','9',a)",
      errors: [
        {
          message: "Unexpected denary method. Consider using e10 instead.",
          type: "CallExpression"
        }
      ]
    },
    {
      code: "x.split('.').map(function (y) { return y + y }).join('$')",
      errors: [
        {
          message: "Unexpected unary method. Consider using e1 instead.",
          type: "CallExpression"
        },
        {
          message: "Unexpected binary method. Consider using e2 instead.",
          type: "CallExpression"
        }
      ]
    }
  ]
})
