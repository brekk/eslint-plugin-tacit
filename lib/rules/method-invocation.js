/**
 * @fileoverview Prefer e0('trim')(x) to x.trim()
 * @author brekk
 */
"use strict"
var F = require("f-utility")
var safePathEq = require("../utils").safePathEq

var arityMap = [
  "nullary",
  "unary",
  "binary",
  "ternary",
  "quaternary",
  "quinary",
  "senary",
  "septenary",
  "octonary",
  "novenary",
  "denary"
]
var entrustMap = F.range(0, 11).map(function(x) {
  return "e" + x
})
//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description: "Prefer e0('trim')(x) to x.trim()",
      category: "Fill me in",
      recommended: false
    },
    fixable: "code", // or "code" or "whitespace"
    schema: [
      // fill in your schema
    ]
  },

  create: function(context) {
    return {
      CallExpression: function CallExpression(node) {
        if (safePathEq("MemberExpression", ["callee", "type"], node)) {
          if (
            safePathEq("Identifier", ["callee", "property", "type"], node) &&
            node.callee.object
          ) {
            var args = node.arguments
            if (arityMap[args.length]) {
              var arity = arityMap[args.length]
              var entrust = entrustMap[args.length]
              context.report({
                node: node,
                loc: node.loc,
                message:
                  "Unexpected " +
                  arity +
                  " method. Consider using " +
                  entrust +
                  " instead.",
                fix: function fix(fixer) {
                  return fixer.replaceTextRange(
                    [
                      node.callee.object.range[0],
                      node.callee.property.range[1]
                    ],
                    entrust +
                      "('" +
                      node.callee.property.name +
                      "', " +
                      (args.length > 0
                        ? args.map(y => y.raw || y.value).join(",") + ","
                        : "") +
                      node.callee.object.name +
                      ")"
                  )
                  // return fixer.replaceText(
                  //   node.callee.property,
                  //   entrust + "('" + node.callee.property.name + "')"
                  // )
                }
              })
            }
          }
        }
      }
    }
  }
}
