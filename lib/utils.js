var F = require("f-utility"),
  curry = F.curry,
  reduce = F.reduce,
  pipe = F.pipe,
  isFunction = F.isFunction,
  pathOr = F.pathOr
var NO_MATCH = "FUTILITY_NO_MATCHING"
var safePathEq = curry(function _safePathEq(compare, steps, x) {
  return pipe(
    reduce(function reductor(last, next) {
      if (last && last[next]) return last[next]
      return NO_MATCH
    }, x),
    function lastOne(y) {
      if (y === NO_MATCH) return false
      return isFunction(compare) ? compare(y) : y === compare
    }
  )(steps)
})

function unexpected(x) {
  return "Unexpected use of " + x + "! Please use Futures instead."
}
var THEN_ERROR = unexpected("then")
var CATCH_ERROR = unexpected("catch")

module.exports = {
  unexpected: unexpected,
  THEN_ERROR: THEN_ERROR,
  CATCH_ERROR: CATCH_ERROR,
  safePathEq: safePathEq
}
