# eslint-plugin-entrust

Encourage tacit programming using `entrust`

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-entrust`:

```
$ npm install eslint-plugin-entrust --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-entrust` globally.

## Usage

Add `entrust` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "entrust"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "entrust/rule-name": 2
    }
}
```

## Supported Rules

* Fill in provided rules here





